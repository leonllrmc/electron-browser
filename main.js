const { app, BrowserWindow, screen, ipcMain, Menu, crashReporter} = require('electron')
const contextMenu = require('electron-context-menu');
const path = require("path")
var mainWindow;

crashReporter.start({
    productName: 'Galaxia',
    companyName: 'Edeltech',
    submitURL: 'http://localhost:3030/galaxia/crash-report',
    uploadToServer: true
})

function createWindow () {
    app.userAgentFallback = app.userAgentFallback.replace('Electron/' + process.versions.electron, '');
    var mainScreen = screen.getPrimaryDisplay();
    var dimensions = mainScreen.size;
    var menu = Menu.buildFromTemplate([])
    Menu.setApplicationMenu(menu);
    var mainWindow = new BrowserWindow({
        width: dimensions.width,
        height: dimensions.height,
        title: app.getName(),
        icon: path.join(__dirname,'icon.png'),
        webPreferences: {
            nodeIntegration: true,
            webviewTag: true,
            BlinkFeatures: false
        }
    })

    mainWindow.loadFile('index.html', {userAgent: 'Chrome'})
}

app.whenReady().then(createWindow)
app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', () => {
    if (mainWindow === null) {
        createWindow()
    }
})
ipcMain.on('quit', (event, arg) => {
    app.quit()
})
ipcMain.on('settings', (event, arg) => {
    let settings = new BrowserWindow({ modal: true, show: false })
    settings.loadFile('settings.html')
    settings.once('ready-to-show', () => {
        settings.show()
    })
})