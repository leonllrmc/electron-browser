var view = document.getElementById('webpage');
var urlInput = document.getElementById('url');
const { ipcRenderer, remote } = require('electron')
const path = require("path")

window.onload = () => {
    //view.setUserAgent('Mozilla/5.0 Chrome/84.0.4147.135 Safari/537.36')
    view.src = localStorage.getItem('search_engine_base_URL')||'https://google.com'
}
/*setInterval(() => {
    document.getElementsByTagName("title").innerHTML(view.getTitle())
},800)*/
function browse() {
        if (IsvalidURL(document.getElementById('url').value)) {
            var url = encodeURI(document.getElementById('url').value)
            if (url.slice(0, 7).toLowerCase() == 'http://' || url.slice(0, 8).toLowerCase() == 'https://') {
                view.src = url;
                document.getElementById('url').blur()
            } else {
                view.src = 'http://' + url;
                document.getElementById('url').blur()
            }

        } else {
            view.src = `${localStorage.getItem('search_engine_URL')||'https://google.com/search?q='}${encodeURIComponent(document.getElementById('url').value)}`
            document.getElementById('url').blur()
        }
}
/*var updateURL = setInterval(() => {
    var urltext = encodeURI(document.getElementById('webpage').src);
    document.getElementById('url').value = urltext;
},5000)*/
function IsvalidURL(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
        '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
        '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
    return !!pattern.test(str);
}
function reload() {
    view.reload();
}

function back() {
    view.goBack();
}

function forward() {
    view.goForward();
}
function toggleDevtools() {
    if (view.isDevToolsOpened()) {
        view.closeDevTools();
    } else {
        view.openDevTools();
    }
}
urlInput.onkeydown = function (event) {
    if (event.keyCode == 13) {
        if (IsvalidURL(document.getElementById('url').value)) {
            var url = encodeURI(document.getElementById('url').value)
            if (url.slice(0, 7).toLowerCase() == 'http://' || url.slice(0, 8).toLowerCase() == 'https://') {
                view.src = url;
                document.getElementById('url').blur()
            } else {
                view.src = 'http://' + url;
                document.getElementById('url').blur()
            }

        } else {
            view.src = `${localStorage.getItem('search_engine_URL')||'https://google.com/search?q='}${encodeURIComponent(document.getElementById('url').value)}`
            document.getElementById('url').blur()
        }
    }
};
urlInput.onblur = function () {
    $('#url').addClass('blur')
};
urlInput.onfocus = function () {
    $('#url').removeClass('blur')
};
$("#fullscreen").click(function () {
    var electron = require("electron")
    var window = electron.remote.getCurrentWindow();
    if(!window.isFullScreen()) {
        window.setFullScreen(true);
    }else{
        window.setFullScreen(false);
    }
})
$("#quit").click(function () {
    ipcRenderer.send("quit")
})
$("#settings").click(function () {
    ipcRenderer.send("settings")
})
document.getElementById('webpage').addEventListener('did-stop-loading', () => {
    document.getElementById('url').value = document.getElementById('webpage').src
})
document.getElementById('webpage').addEventListener('did-navigate', () => {
    document.getElementById('url').value = document.getElementById('webpage').src
})
document.getElementById('webpage').addEventListener('dom-ready', () => {
    document.getElementById('url').value = document.getElementById('webpage').src
})
document.getElementById('webpage').addEventListener('enter-html-full-screen', () => {
    $(".navbar").hide()
})
document.getElementById('webpage').addEventListener('leave-html-full-screen', () => {
    $(".navbar").show()
})
/*document.getElementById('webpage').addEventListener('did-fail-load', () => {
    view.src = path.join(__dirname,'error/index.html')
})*/